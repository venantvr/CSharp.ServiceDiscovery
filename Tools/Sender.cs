using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web.Script.Serialization;
using Contracts;

namespace Tools
{
    public class Sender : IDisposable, ISender
    {
        private readonly IPEndPoint _localEp;
        private readonly JavaScriptSerializer _serializer;
        private readonly UdpClient _udpClient;

        public Sender()
        {
            _serializer = new JavaScriptSerializer();

            _udpClient = new UdpClient(); // { ExclusiveAddressUse = false, EnableBroadcast = true, MulticastLoopback = false }; // Ajout de properties
            var multicastaddress = IPAddress.Parse("239.0.0.222");
            _udpClient.JoinMulticastGroup(multicastaddress);
            _localEp = new IPEndPoint(multicastaddress, 2222);
        }

        public void Dispose()
        {
            _udpClient.Close();
        }

        public void Send<T>(T what)
        {
            var serializedResult = _serializer.Serialize(what);

            var buffer = Encoding.Unicode.GetBytes(serializedResult);
            _udpClient.Send(buffer, buffer.Length, _localEp);
            Console.WriteLine("=> : " + serializedResult);
        }
    }
}