using System.Collections.Generic;
using System.Linq;
using Contracts;

namespace Tools
{
    public class InMemoryIdentitiesStore : IIdentitiesStore
    {
        private readonly Dictionary<string, string> _store;

        public InMemoryIdentitiesStore()
        {
            _store = new Dictionary<string, string>();
        }

        public List<IIdentityCard> Resolve()
        {
            var cards = new List<IIdentityCard>();

            var hostNames = _store.Values.ToList().Distinct();

            foreach (var hostName in hostNames)
            {
                cards.Add(new IdentityCard { HostName = hostName, Routes = _store.Where(s => s.Value == hostName).Select(s => s.Key).ToArray() });
            }

            return cards;
        }

        public void Add(string route, string hostName)
        {
            if (!_store.ContainsKey(route))
            {
                _store.Add(route, hostName);
            }
        }
    }
}