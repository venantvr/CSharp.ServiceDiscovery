using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Contracts;
using Tools;

namespace Probe
{
    internal class MainClass
    {
        private static void Main(string[] args)
        {
            var inMemoryIdentitiesStore = new InMemoryIdentitiesStore();

            Func<IIdentityCard> identityResolver = () => new IdentityCard { HostName = /*Dns.GetHostName()*/ Guid.NewGuid().ToString(), Routes = new[] { @"/hello/", @"/bye/" } };

            Func<List<IIdentityCard>> identitiesStore = inMemoryIdentitiesStore.Resolve;

            Action<List<IdentityCard>> identitiesPersister = w =>
                                                             {
                                                                 foreach (var item in w)
                                                                 {
                                                                     foreach (var route in item.Routes)
                                                                     {
                                                                         inMemoryIdentitiesStore.Add(route, item.HostName);
                                                                     }
                                                                 }
                                                             };

            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;

            Task.Factory.StartNew(() =>
                                  {
                                      using (var sender = new Sender())
                                          sender.Send(new Message
                                                      {
                                                          Operation = Operation.Sync,
                                                          ServicesCards = new List<IdentityCard> { identityResolver.Invoke() as IdentityCard }
                                                      });
                                  }
                , cancellationToken)
                .ContinueWith(antecedent => Task.Delay(100, cancellationToken), cancellationToken)
                .ContinueWith(antecedent =>
                              {
                                  while (!cancellationToken.WaitHandle.WaitOne(0))
                                  {
                                      using (var receiver = new Receiver())
                                      {
                                          var data = receiver.Receive<Message>();

                                          identitiesPersister.Invoke(data.ServicesCards);

                                          if (data.Operation == Operation.Sync)
                                          {
                                              using (var sender = new Sender())
                                                  sender.Send(new Message { Operation = Operation.Ack, ServicesCards = identitiesStore.Invoke().Cast<IdentityCard>().ToList() });
                                          }
                                      }
                                  }
                              }
                    , cancellationToken);

            Console.ReadLine();
            Console.WriteLine("All Done! Press ENTER to quit.");

            cancellationTokenSource.Cancel();
        }
    }
}